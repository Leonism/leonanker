# Overview

Welcome to my personal portfolio.

My portfolio website is hosted and deployed on AWS, utilizing services such as S3, CloudFront, Lambda, and API Gateway. This hands-on cloud project demonstrates fundamental skills in modern cloud infrastructure, including Infrastructure as Code (Terraform), CI/CD automation, and serverless computing.

## [Live Portfolio 🔗](https://leon-anker.com)

## Architecture

![Architecture Diagram](/img/architecture.jpg)

**Technologies Used**:

- S3
- AWS CloudFront
- Certificate Manager
- AWS Lambda
- Dynamo DB
- CI/CD
- Terraform (Infrastructure as Code)